import React, { Component } from 'react';
import logo from './logo.svg';
import Todolist from './Todolist'
import Inputfield from './Todoinput'
import id from 'uuid';
import './App.css';


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      listItem: []
    }
  }

  addList = (data) => {
    if (data.length !== 0) {
      let objectData = {};
      objectData["value"] = data;
      objectData["_id"] = id.v4();
      objectData["status"] = false;
      this.setState({ listItem: this.state.listItem.concat([objectData]) })
    }
  }
  removeElement = (itemId) => {
    let items = this.state.listItem
    let filterArray = items.filter(item => {
      return item._id !== (itemId)
    });
    this.setState({ listItem: filterArray });
  }
  checkList = (itemId) => {
    let items = this.state.listItem;
    let filterData = items.filter(item => {
      if (item._id == itemId) {
        if (item.status) {
          item.status = false;
        } else {
          item.status = true;
        }
      }
      return item
    });
    this.setState({ listItem: filterData });
  }
  render() {
    return (
      <div className="App">
        <header className="header-container">
          To-Do List
        </header>
        <Inputfield inputList={this.state.listItem} addItem={this.addList} />
        <Todolist inputList={this.state.listItem} deleteElement={this.removeElement} checkStatus={this.checkList} />
      </div>
    );
  }
}

export default App;
