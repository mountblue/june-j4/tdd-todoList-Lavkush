import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import config from './setUp';
import TodoListInput from './Todoinput';
describe('Input test cases', () => {
    it('element should be renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<TodoListInput />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('should be a butoon and a input ', () => {
        let wrapper = mount(<TodoListInput />)
        expect(wrapper.find('input').length).toEqual(1);
        expect(wrapper.find('button').length).toEqual(1);
    });


    it('only one input element should  be hear', () => {
        let wrapper = mount(<TodoListInput />)
        expect(wrapper.find('input').length).toBe(1);
    });

    it('input element should not have value', () => {
        let wrapper = mount(<TodoListInput />)
        expect(wrapper.find('input').text()).toBe("");
    });

    it('should call check function when check is clicked', () => {
        const spy = jest.fn();
        let item = { value: "hi" };
        const wrapper = mount(<TodoListInput addItem={spy} inputList={[item]} />);
        const checkList = wrapper.find('button');
        checkList.simulate('click');
        expect(spy).toHaveBeenCalledTimes(1);
    });
});