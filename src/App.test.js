import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { shallow, mount } from 'enzyme';
import config from './setUp';
import { spy } from 'sinon';

describe('test cases', () => {

  it('element should be renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('state should be empty', () => {
    let wrapper = mount(<App />)
    expect(wrapper.state('listItem')).toEqual([]);
  });

  it('input should be empty', () => {
    let wrapper = mount(<App />)
    expect(wrapper.find('Inputfield').length).toEqual(0);
  });

  it('add items in function', () => {
    let wrapper = shallow(<App />)
    wrapper.instance().addList("ok")
    expect(wrapper.state('listItem')[0].value).toEqual("ok"
    );
  });

  it('should be delete items from function', () => {
    let wrapper = shallow(<App />)
    wrapper.instance().addList("ok")
    let itemId = wrapper.state('listItem')[0]._id
    wrapper.instance().removeElement(itemId)
    expect(wrapper.state('listItem')).toEqual([]);
  });

  it('should be change status in function', () => {
    let wrapper = shallow(<App />)
    wrapper.instance().addList("ok")
    let itemId = wrapper.state('listItem')[0]._id
    wrapper.instance().checkList(itemId)
    expect(wrapper.state('listItem')[0].status).toEqual(true);
  });
});