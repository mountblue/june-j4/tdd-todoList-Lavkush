import React from 'react';
import PropTypes from 'prop-types';


function InputField(props) {
    function addItem(e) {
        let inputText = e.target.previousSibling.value;
        props.addItem(inputText)
        e.target.previousSibling.value="";
    }
    return (
        <div >
            <input className="tp" name="nm" onfocus="" />
            <button onClick={addItem} > Add List </button>
        </div>
    );
}
export default InputField;
