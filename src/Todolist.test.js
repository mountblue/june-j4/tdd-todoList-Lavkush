import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import config from './setUp';
import TodoListItem from './Todolist'
import { spy } from 'sinon';
describe('list test cases', () => {
    it('element should be renders without crashing', () => {
        const spy = jest.fn();
        let item = { value: "hi" };
        const div = document.createElement('div');
        ReactDOM.render(<TodoListItem inputList={[item]} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('should not be any task at starting', () => {
        let testItemList = []
        let wrapper = shallow(<TodoListItem inputList={testItemList} />)
        expect(wrapper.find('li').length).toBe(0);
    });

    it('should not be any task in starting', () => {
        let testItemList = [{ value: "lav" }]
        let wrapper = shallow(<TodoListItem inputList={testItemList} />)
        expect(wrapper.find('li').length).toBe(1);
    });

    it('should be call delete function when delete is clicked', () => {
        const spy = jest.fn();
        let item = { value: "hi" };
        const wrapper = mount(<TodoListItem deleteElement={spy} inputList={[item]} />);
        const deleteButton = wrapper.find('button');
        deleteButton.simulate('click');
        expect(spy).toHaveBeenCalledTimes(1);
    });


    it('should be call check function when check is clicked', () => {
        const spy = jest.fn();
        let item = { value: "hi" };
        const wrapper = mount(<TodoListItem checkStatus={spy} inputList={[item]} />);
        const checkButton = wrapper.find('input');
        checkButton.simulate('click');
        expect(spy).toHaveBeenCalledTimes(1);
    });
})