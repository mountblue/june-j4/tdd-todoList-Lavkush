import React from 'react';
import PropTypes from 'prop-types';
import './App.css';
function Todolist(props) {

    function deleteElement(event) {
        props.deleteElement(event.target.id);
    }
    function checkStatus(event) {
        props.checkStatus(event.target.id);
    }
    let element = props.inputList;
    return (
        <div>
            {
                props.inputList.map((item,index) => {
                    return (
                        <div key={index+"j"}>
                            <input key={index+"1"} type="checkbox" id={item._id} onClick={checkStatus} />
                            <li key={index}  className={item.status?"checked":"unchecked"} >{item.value}</li>
                            <button key={index+"k"} id={item._id} onClick={deleteElement}>delete</button>
                        </div>
                    )
                })
            }
        </div>
    );


}
export default Todolist;
